<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        User::factory()->create(10);

         User::factory()->create([
             'email' => 'antmedia@gmail.com',
             'password' => bcrypt(123456), // password 123456
         ]);
    }
}
