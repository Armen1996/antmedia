# пересобирает контейнеры подготавливает базу для дев разработки
init: rebuild composer-install fresh-migrate passport-install passport-passport-keys

# собирает контейнеры
up:
	docker-compose -f docker-compose.yml up -d
# Останавливает контейнеры и удаляет контейнеры, сети, тома и образы, созданные через up
down:
	docker-compose -f docker-compose.yml down --remove-orphans
# пересобрать или перезапустить контейнеры
rebuild:
	docker-compose -f docker-compose.yml up -d --build
# перейти в консоль контейнера с php
bash:
	docker exec -it ant-media-server-php bash

# команды для инициализации dev среды
composer-install:
	docker exec -t  ant-media-server-php composer install
fresh-migrate:
	docker exec -t  ant-media-server-php php artisan migrate:fresh
passport-install:
	docker exec -t  ant-media-server-php  php artisan passport:install
passport-passport-keys:
	docker exec -t  ant-media-server-php  php artisan passport:keys --force

