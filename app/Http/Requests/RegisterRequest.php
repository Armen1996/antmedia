<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    public const EMAIL = 'email';
    public const PASSWORD = 'password';
    public const LANGUAGE = 'language';
    public const DISPLAY_NAME = 'display_name';
    public const TELEGRAM_USERNAME = 'telegram_username';

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            self::EMAIL => [
                'string',
                'required',
                'unique:users',
                'max:255',
            ],
            self::PASSWORD => [
                'string',
                'required',
                'min:5',
            ],
            self::DISPLAY_NAME => [
                'nullable',
                'string',
                'between:2,255',
            ],
            self::TELEGRAM_USERNAME => [
                'string',
                'nullable',
            ],
            self::LANGUAGE => [
                'string',
                'nullable',
            ]
        ];
    }

    public function getEmail(): string
    {
        return $this->get(self::EMAIL);
    }

    public function getPassword(): string
    {
        return $this->get(self::PASSWORD);
    }

    public function getDisplayName(): ?string
    {
        return $this->get(self::DISPLAY_NAME);
    }

    public function getTelegramUsername(): ?string
    {
        return $this->get(self::TELEGRAM_USERNAME);
    }

    public function getLanguage(): ?string
    {
        return $this->get(self::LANGUAGE);
    }
}
