<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\JsonResponse;
use App\Services\User\Dto\LoginDto;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Nyholm\Psr7\Factory\Psr17Factory;
use App\Http\Requests\RegisterRequest;
use App\Services\User\Dto\CreateUserDto;
use App\Http\Resources\User\UserResource;
use App\Http\Requests\RefreshTokenRequest;
use App\Services\User\Actions\LoginAction;
use App\Http\Resources\User\TokensResource;
use App\Services\User\Actions\LogoutAction;
use Illuminate\Auth\AuthenticationException;
use App\Services\User\Actions\CreateUserAction;
use App\Services\User\Actions\RefreshTokenAction;
use Illuminate\Auth\Access\AuthorizationException;
use Laravel\Passport\Exceptions\OAuthServerException;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class AuthController extends Controller
{
    /**
     * @throws UnknownProperties
     */
    public function register(
        RegisterRequest $request,
        CreateUserAction $createUserAction
    ): UserResource {
        $dto = CreateUserDto::fromRequest($request);
        $user = $createUserAction->run($dto);

        return new UserResource($user);
    }

    /**
     * @throws UnknownProperties|AuthorizationException
     * @throws OAuthServerException
     */
    public function login(
        LoginRequest $request,
        LoginAction $loginAction
    ): TokensResource {
        $dto = LoginDto::fromRequest($request);

        $serverRequest = (new PsrHttpFactory(
            new Psr17Factory(),
            new Psr17Factory(),
            new Psr17Factory(),
            new Psr17Factory()
        ))->createRequest($request);

        return $loginAction->run($dto, $serverRequest);
    }

    /**
     * @throws AuthenticationException
     */
    public function logout(LogoutAction $logoutAction): JsonResponse
    {
        $user = Auth::user();

        $logoutAction->run($user);

        return $this->response([
            'success' => true
        ]);
    }

    /**
     * @throws AuthorizationException
     * @throws OAuthServerException
     */
    public function refresh(
        RefreshTokenRequest $request,
        RefreshTokenAction $refreshTokenAction
    ): TokensResource {
        $serverRequest = (new PsrHttpFactory(
            new Psr17Factory(),
            new Psr17Factory(),
            new Psr17Factory(),
            new Psr17Factory()
        ))->createRequest($request);

        return $refreshTokenAction->run(
            $request->getRefreshToken(),
            $serverRequest
        );
    }
}
