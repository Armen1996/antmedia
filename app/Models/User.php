<?php

namespace App\Models;

use Database\Factories\UserFactory;
use Laravel\Passport\HasApiTokens;
use App\Services\User\Dto\CreateUserDto;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property string|null $display_name
 * @property string|null $telegram_username
 * @property string $password
 * @property mixed $id
 * @property string $email
 */
class User extends Authenticatable
{
    use HasFactory;
    use Notifiable;
    use HasApiTokens;

    protected $fillable = [
        'email',
        'password',
        'display_name',
        'telegram_username',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function createUser(CreateUserDto $dto): User
    {
        $user = new static();

        $user->setUsername($dto->email);
        $user->setDisplayName($dto->displayName);
        $user->setTelegramUsername($dto->telegramUsername);
        $user->setPassword($dto->password);

        return $user;
    }

    public function setUsername(string $email): void
    {
        $this->email = $email;
    }

    public function setDisplayName(?string $displayName): void
    {
        $this->display_name = $displayName;
    }

    public function setTelegramUsername(?string $telegramUsername): void
    {
        $this->telegram_username = $telegramUsername;
    }

    public function setPassword(string $password): void
    {
        $this->password = bcrypt($password);
    }
}
