<?php

namespace App\Services\User\Actions;

use App\Models\User;
use App\Events\User\LogoutEvent;
use Illuminate\Auth\AuthenticationException;

class LogoutAction
{
    /**
     * @throws AuthenticationException
     */
    public function run(?User $user): void
    {
        if (is_null($user)) {
            throw new AuthenticationException();
        }

        $token = $user->token();
        $token->revoke();

        LogoutEvent::dispatch($user->id);
    }
}
