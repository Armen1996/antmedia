<?php

namespace App\Services\User\Actions;

use App\Models\User;
use App\Events\User\RegisterEvent;
use App\Services\User\Dto\CreateUserDto;
use App\Repositories\Write\User\UserWriteRepositoryInterface;

class CreateUserAction
{
    public function __construct(
        private readonly UserWriteRepositoryInterface $userWriteRepository,
    ) {
    }

    public function run(CreateUserDto $dto): User
    {
        $user = User::createUser($dto);
        $this->userWriteRepository->save($user);

        RegisterEvent::dispatch($user->id, $dto);

        return $user;
    }
}
