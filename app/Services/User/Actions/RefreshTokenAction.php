<?php

namespace App\Services\User\Actions;

use Laravel\Passport\Client;
use Nyholm\Psr7\Response as Psr7Response;
use App\Http\Resources\User\TokensResource;
use Psr\Http\Message\ServerRequestInterface;
use League\OAuth2\Server\AuthorizationServer;
use Illuminate\Auth\Access\AuthorizationException;
use Laravel\Passport\Exceptions\OAuthServerException;
use App\Repositories\Read\UserReadRepositoryInterface;
use Laravel\Passport\Http\Controllers\HandlesOAuthErrors;

class RefreshTokenAction
{
    use HandlesOAuthErrors;

    public function __construct(
        private readonly AuthorizationServer $server,
        private readonly UserReadRepositoryInterface $userReadRepository,
    ) {
    }

    /**
     * @throws AuthorizationException
     * @throws OAuthServerException
     */
    public function run(string $refreshToken, ServerRequestInterface $serverRequest): TokensResource
    {
        $oClientId = config('passport.personal_access_client.id');
        $oClient = Client::where('id', $oClientId)->first();

        $serverRequest = $serverRequest->withParsedBody([
            'grant_type' => 'refresh_token',
            'client_id' => $oClient->id,
            'client_secret' => $oClient->secret,
            'refresh_token' => $refreshToken,
            'scope' => '',
        ]);

        $response = $this->withErrorHandling(function () use ($serverRequest) {
            return $this->convertResponse(
                $this->server->respondToAccessTokenRequest($serverRequest, new Psr7Response)
            );
        });

        $data = $response->getContent();

        if (is_null($data = json_decode($data))) {
            throw new AuthorizationException();
        }

        return new TokensResource($data);
    }
}
