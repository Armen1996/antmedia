<?php

namespace App\Services\User\Dto;

use App\Http\Requests\LoginRequest;
use Spatie\DataTransferObject\DataTransferObject;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class LoginDto extends DataTransferObject
{
    public string $ip;
    public string $email;
    public string $password;

    /**
     * @throws UnknownProperties
     */
    public static function fromRequest(LoginRequest $request): LoginDto
    {
        return new self(
            email: $request->getUserName(),
            password: $request->getPassword(),
            ip: $request->ip(),
        );
    }
}
