<?php

namespace App\Services\User\Dto;

use App\Http\Requests\RegisterRequest;
use Spatie\DataTransferObject\DataTransferObject;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class CreateUserDto extends DataTransferObject
{
    public string $email;
    public string $password;
    public ?string $language;
    public ?string $displayName;
    public ?string $telegramUsername;

    /**
     * @throws UnknownProperties
     */
    public static function fromRequest(RegisterRequest $request): CreateUserDto
    {
        return new self(
            email: $request->getEmail(),
            password: $request->getPassword(),
            language: $request->getLanguage(),
            displayName: $request->getDisplayName(),
            telegramUsername: $request->getTelegramUsername(),
        );
    }
}
