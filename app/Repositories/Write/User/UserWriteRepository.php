<?php

namespace App\Repositories\Write\User;

use App\Models\User;
use App\Exceptions\User\UserSavingErrorException;

class UserWriteRepository implements UserWriteRepositoryInterface
{

    /**
     * @throws UserSavingErrorException
     */
    public function save(User $user): User
    {
        if (!$user->save()) {
            throw new UserSavingErrorException();
        }

        return $user;
    }
}
