<?php

namespace App\Repositories\Read;

use App\Models\User;

interface UserReadRepositoryInterface
{
    public function getByEmail(string $username, array $relations = []): User;
}
