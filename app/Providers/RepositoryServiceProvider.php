<?php

namespace App\Providers;

use App\Repositories\Read\UserReadRepository;
use App\Repositories\Write\User\UserWriteRepository;
use App\Repositories\Read\UserReadRepositoryInterface;
use App\Repositories\Write\User\UserWriteRepositoryInterface;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(
            UserWriteRepositoryInterface::class,
            UserWriteRepository::class
        );

        $this->app->bind(
            UserReadRepositoryInterface::class,
            UserReadRepository::class
        );
    }
}
